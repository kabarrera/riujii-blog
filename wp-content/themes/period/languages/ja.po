# Copyright (C) 2016 Compete Themes
# This file is distributed under the GNU General Public License v2 or later.
msgid ""
msgstr ""
"Project-Id-Version: Period 1.04\n"
"Report-Msgid-Bugs-To: http://wordpress.org/support/theme/style\n"
"POT-Creation-Date: 2016-10-08 23:38+0900\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"PO-Revision-Date: 2016-10-09 00:27+0900\n"
"Language-Team: \n"
"X-Generator: Poedit 1.8.7.1\n"
"Last-Translator: Masayuki Sugahara <masayuki@sss.rxz.jp>\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"Language: ja\n"

#: 404.php:6
msgid "404: Page Not Found"
msgstr "404: ページが見つかりません"

#: 404.php:9
msgid ""
"Looks like nothing was found on this url.  Double-check that the url is "
"correct or try the search form below to find what you were looking for."
msgstr ""
"どうやらこのURLでは何も見つからなかったようです。URLが正しいかどうか確認して"
"ください。検索もお試しください。"

#: comments.php:12 comments.php:32 comments.php:54
msgid "Be First to Comment"
msgstr "コメントする"

#: comments.php:12 comments.php:32 comments.php:54
msgid "One Comment"
msgstr "1件のコメント"

#: comments.php:12 comments.php:32 comments.php:54 content/comments-link.php:5
#: content/comments-link.php:8
msgid "% Comments"
msgstr "%件のコメント"

#: comments.php:46
msgid ""
"Comments are closed, but <a href=\"%s\" title=\"Trackback URL for this post"
"\">trackbacks</a> and pingbacks are open."
msgstr ""
"コメントはクローズされていますが、<a href=\"%s\" title=\"この記事のトラック"
"バックURL\">トラックバック</a> と pingback はご利用になれます。"

#: comments.php:68 comments.php:75
msgid "Comments are closed."
msgstr "コメントはクローズされています。"

#: content/archive-header.php:4
msgid "Posts"
msgstr "記事"

#: content/comments-link.php:2
msgid "comment icon"
msgstr "コメントアイコン"

#: content/comments-link.php:5
msgid "Comments closed"
msgstr "コメントはクローズされています"

#: content/comments-link.php:5 content/comments-link.php:8
msgid "1 Comment"
msgstr "1件のコメント"

#: content/comments-link.php:8
msgid "Leave a Comment"
msgstr "コメントする"

#: content/post-categories.php:10
msgid "Published in"
msgstr "カテゴリー"

#: content/post-categories.php:17
msgid "View all posts in %s"
msgstr "%s のすべての記事を見る"

#: content/post-nav-attachment.php:3
msgid "Previous Image"
msgstr "前の画像"

#: content/post-nav-attachment.php:6
msgid "Next Image"
msgstr "次の画像"

#: content/post-nav.php:6
msgid "Previous Post"
msgstr "前の記事"

#: content/post-nav.php:9
msgid "No Older Posts"
msgstr "これより古い記事はありません"

#: content/post-nav.php:15 content/post-nav.php:28
msgid "Return to Blog"
msgstr "ブログに戻る"

#: content/post-nav.php:19
msgid "Next Post"
msgstr "次の記事"

#: content/post-nav.php:22
msgid "No Newer Posts"
msgstr "これより新しい記事はありません"

#: content/post-tags.php:8
msgid "View all posts tagged %s"
msgstr "%s タグに関するすべての記事を見る"

#: content/search-bar.php:11 searchform.php:3 searchform.php:6
msgid "Search"
msgstr "検索"

#: content/search-bar.php:12
msgid "Search..."
msgstr "検索…"

#: content/search-bar.php:13 searchform.php:5
msgid "Search for:"
msgstr "キーワード:"

#: content-page.php:12 content.php:13
msgid "Pages:"
msgstr "ページ:"

#: footer.php:15
msgid "<a href=\"%s\">Period WordPress Theme</a> by Compete Themes."
msgstr "<a href=\"%s\">Period WordPress Theme</a> by Compete Themes."

#: functions.php:32
msgid "Primary"
msgstr "主"

#: functions.php:43
msgid "Primary Sidebar"
msgstr "主サイドバー"

#: functions.php:45
msgid ""
"Widgets in this area will be shown in the sidebar next to the main post "
"content"
msgstr ""
"このエリアにあるウィジェットはメイン記事コンテンツの隣にあるサイドバーに表示"
"されます。"

#: functions.php:72
msgid "Your comment is awaiting moderation."
msgstr "コメントは承認待ちです。"

#: functions.php:79
msgid "Reply"
msgstr "返信"

#: functions.php:84
msgid "Edit"
msgstr "編集"

#: functions.php:96
msgid "(optional)"
msgstr "(省略可能)"

#: functions.php:101
msgid "Name"
msgstr "名前"

#: functions.php:102
msgid "Jane Doe"
msgstr "山田 稔"

#: functions.php:108
msgid "Email"
msgstr "メールアドレス"

#: functions.php:109
msgid "name@email.com"
msgstr "name@email.com"

#: functions.php:115
msgid "Website"
msgstr "ウェブサイト"

#: functions.php:130
msgid "Comment"
msgstr "コメント"

#: functions.php:161 functions.php:170 functions.php:186
msgid "Continue reading"
msgstr "続きを読む"

#: functions.php:329 functions.php:330
msgid "email"
msgstr "メール"

#: functions.php:381 inc/scripts.php:12
msgid "open dropdown menu"
msgstr "ドロップダウンメニューを開く"

#: functions.php:392
msgid "Featured"
msgstr "注目"

#: functions.php:450
msgid "Customizer settings deleted"
msgstr "カスタマイズ設定は削除されました"

#: header.php:11
msgid "Press \"Enter\" to skip to content"
msgstr "コンテンツにスキップするには Enter キーを押してください"

#: header.php:23 inc/scripts.php:10
msgid "open menu"
msgstr "メニューを開く"

#: inc/customizer.php:15
msgid "Front Page"
msgstr "メインページ"

#: inc/customizer.php:27
msgid "Logo"
msgstr "ロゴ"

#: inc/customizer.php:37
msgid "Upload a logo"
msgstr "ロゴをアップロード"

#: inc/customizer.php:50
msgid "Adjust the size of the logo"
msgstr "ロゴのサイズを調整"

#: inc/customizer.php:71
msgid "Social Media Icons"
msgstr "ソーシャルメディアアイコン"

#: inc/customizer.php:73
msgid "Add the URL for each of your social profiles."
msgstr "各 SNS アカウントへの URL を追加してください。"

#: inc/customizer.php:86
msgid "Email Address"
msgstr "メールアドレス"

#: inc/customizer.php:135
msgid ""
"Accepts Skype link protocol (<a href=\"%s\" target=\"_blank\">learn more</a>)"
msgstr ""
"Skype リンクプロトコルがご利用いただけます (<a href=\"%s\" target=\"_blank\">"
"詳細</a>)"

#: inc/customizer.php:161
msgid "Search Bar"
msgstr "検索バー"

#: inc/customizer.php:172
msgid "Show search bar at top of site?"
msgstr "サイト上部に検索バーを表示"

#: inc/customizer.php:176 inc/customizer.php:275 inc/customizer.php:291
#: inc/customizer.php:326 inc/customizer.php:370
msgid "Show"
msgstr "表示"

#: inc/customizer.php:177 inc/customizer.php:276 inc/customizer.php:292
#: inc/customizer.php:327 inc/customizer.php:371
msgid "Hide"
msgstr "非表示"

#: inc/customizer.php:185
msgid "Layout"
msgstr "レイアウト"

#: inc/customizer.php:187
msgid ""
"Want more layouts? Check out the <a target=\"_blank\" href=\"%s\">Period Pro "
"plugin</a>."
msgstr ""
"他のレイアウトも試してみませんか？ <a target=\"_blank\" href=\"%s\">Period "
"Pro プラグイン</a> をチェックしてください。"

#: inc/customizer.php:197
msgid "Choose your layout"
msgstr "レイアウトを選択"

#: inc/customizer.php:202 inc/customizer.php:385
msgid "Right sidebar"
msgstr "右サイドバー"

#: inc/customizer.php:203 inc/customizer.php:386
msgid "Left sidebar"
msgstr "左サイドバー"

#: inc/customizer.php:211
msgid "Blog"
msgstr "ブログ"

#: inc/customizer.php:221
msgid "Show full posts on blog?"
msgstr "記事全体を表示"

#: inc/customizer.php:226 inc/customizer.php:345
msgid "Yes"
msgstr "はい"

#: inc/customizer.php:227 inc/customizer.php:346
msgid "No"
msgstr "いいえ"

#: inc/customizer.php:237
msgid "Excerpt word count"
msgstr "概略の長さ"

#: inc/customizer.php:244
msgid "Continue Reading"
msgstr "続きを読む"

#: inc/customizer.php:249
msgid "Read More button text"
msgstr "[続きを読む]ボタンのテキスト"

#: inc/customizer.php:259
msgid "Display Controls"
msgstr "表示"

#: inc/customizer.php:261
msgid ""
"Want more options like these? Check out the <a target=\"_blank\" href=\"%s"
"\"> Period Pro plugin</a>."
msgstr ""
"他の選択肢も試してみませんか？ <a target=\"_blank\" href=\"%s\">Period Pro プ"
"ラグイン</a> をチェックしてください。"

#: inc/customizer.php:271
msgid "Post author name in byline"
msgstr "記事に投稿者の名前を表示"

#: inc/customizer.php:287
msgid "Post date in byline"
msgstr "記事に投稿日を表示"

#: inc/customizer.php:300
msgid "Custom CSS"
msgstr "カスタム CSS"

#: inc/customizer.php:311
msgid "Add Custom CSS Here"
msgstr "ここにカスタム CSS を記述できます"

#: inc/customizer.php:387
msgid "No sidebar - Narrow"
msgstr "サイドバー無 - 狭い"

#: inc/customizer.php:388
msgid "No sidebar - Wide"
msgstr "サイドバー無 - 広い"

#: inc/customizer.php:389
msgid "Two column - Right sidebar"
msgstr "2カラム - 右サイドバー"

#: inc/customizer.php:390
msgid "Two column - Left sidebar"
msgstr "2カラム - 左サイドバー"

#: inc/customizer.php:391
msgid "Two column - No Sidebar - Narrow"
msgstr "2カラム - サイドバー無 - 狭い"

#: inc/customizer.php:392
msgid "Two column - No Sidebar - Wide"
msgstr "2カラム - サイドバー無 - 広い"

#: inc/customizer.php:402
msgid "View the Period Pro Plugin"
msgstr "Period Pro プラグインを参照"

#: inc/scripts.php:11
msgid "close menu"
msgstr "メニューを閉じる"

#: inc/scripts.php:13
msgid "close dropdown menu"
msgstr "ドロップダウンメニューを閉じる"

#: index.php:21
msgid "Previous"
msgstr "前へ"

#: index.php:22
msgid "Next"
msgstr "次へ"

#: search.php:9
msgid "%d search result for \"%s\""
msgid_plural "%d search results for <span class=\"query\">\"%s\"</span>"
msgstr[0] ""
"%d 件の結果が<span class=\"query\">\"%s\"</span> に関して見つかりました"

#: search.php:11
msgid "No search results for <span class=\"query\">\"%s\"</span>"
msgstr ""
"<span class=\"query\">\"%s\"</span> で検索しましたが見つかりませんでした"

#: search.php:34
msgid "Can't find what you're looking for?  Try refining your search:"
msgstr ""
"お探しのものが見つかりませんでしたか？ 他のキーワードも試してみてください。"

#: searchform.php:5
msgid "Search for..."
msgstr "検索…"

#: sidebar-primary.php:3
msgid "Sidebar"
msgstr "サイドバー"

#: theme-options.php:4 theme-options.php:19
msgid "Period Dashboard"
msgstr "Period ダッシュボード"

#: theme-options.php:22
msgid "Customization"
msgstr "カスタマイズ"

#: theme-options.php:23
msgid ""
"Click the \"Customize\" link in your menu, or use the button below to get "
"started customizing Period"
msgstr ""
"メニューの[カスタマイズ]ボタンか、下のボタンをクリックして Period のカスタマ"
"イズをはじめましょう。"

#: theme-options.php:26
msgid "Use Customizer"
msgstr "カスタマイズ"

#: theme-options.php:30
msgid "Support"
msgstr "サポート"

#: theme-options.php:31
msgid ""
"You can find the knowledgebase, changelog, support forum, and more in the "
"Period Support Center"
msgstr ""
"Period サポートセンターでは、ドキュメント、変更履歴、フォーラム、その他の情報"
"にアクセスできます。"

#: theme-options.php:34
msgid "Visit Support Center"
msgstr "サポートセンターへ"

#: theme-options.php:38
msgid "Period Pro"
msgstr "Period Pro"

#: theme-options.php:39
msgid ""
"Download the Period Pro plugin and unlock custom colors, new layouts, "
"sliders, and more"
msgstr ""
"Period Pro プラグインをダウンロードして、カスタムカラー、追加のレイアウト、ス"
"ライダーその他を有効化しましょう。"

#: theme-options.php:42
msgid "See Full Feature List"
msgstr "すべての機能を見る"

#: theme-options.php:46
msgid "WordPress Resources"
msgstr "WordPress リソース"

#: theme-options.php:47
msgid ""
"Save time and money searching for WordPress products by following our "
"recommendations"
msgstr "WordPress 製品のおすすめを確認して、時間とお金を節約しましょう。"

#: theme-options.php:50
msgid "View Resources"
msgstr "リソースを見る"

#: theme-options.php:54 theme-options.php:56
msgid "Leave a Review"
msgstr "レビューを書く"

#: theme-options.php:55
msgid "Help others find Period by leaving a review on wordpress.org."
msgstr ""
"wordpress.org でレビューを書いて、他の人がこのテーマを見つけやすくできます。"

#: theme-options.php:59 theme-options.php:67
msgid "Reset Customizer Settings"
msgstr "カスタマイズ設定をリセット"

#: theme-options.php:61
msgid ""
"<strong>Warning:</strong> Clicking this button will erase the Period theme's "
"current settings in the <a href='%s'>Customizer</a>."
msgstr ""
"<strong>警告:</strong> このボタンをクリックすると、現在 Period テーマに加えら"
"れている<a href='%s'>カスタマイズ</a>はすべて消去されます。"

#. Description of the plugin/theme
msgid ""
"Period displays your content beautifully across phones, widescreen monitors, "
"and everything in between. With special attention to typography, "
"accessibility, and space, Period is a design you can trust for displaying "
"your content."
msgstr ""
"Period はスマートフォンでも、大きなモニタでも、その中間のどんな画面でも、コン"
"テンツを美しく表示することを考えて作られました。タイポグラフィ、アクセシビリ"
"ティ、そして余白に細心の注意を払っていますので、デザインは Period に任せて、"
"コンテンツの質に集中していただけます。"

#: content/post-byline.php:14
msgctxt "This blog post was published on some date"
msgid "Published %s"
msgstr "投稿日 %s"

#: content/post-byline.php:16
msgctxt "This blog post was published by some author"
msgid "Published by %s"
msgstr "投稿者 %s"

#: content/post-byline.php:18
msgctxt "This blog post was published on some date by some author"
msgid "Published %1$s by %2$s"
msgstr "%1$s に %2$s が投稿しました"
